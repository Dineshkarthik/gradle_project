Feature: Login and create a lead in leaftaps
#Background:
#Given open the browser
#And enter the url
@smoke
Scenario Outline: TC001_Create Lead with mandatory fields
And enter the username as <username>
And enter the Password as <password>
And click login button
And click crmsfa
And click leads
And click create lead link
And enter company name as <company name>
And enter firstname as <first name>
And enter lastname as <last name> 
When click submit button
Then Lead is created
Examples:
|username|password|company name|first name|last name|
|DemoSalesManager|crmsfa|Raw|Kurt|Angle|