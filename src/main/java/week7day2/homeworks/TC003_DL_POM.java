package week7day2.homeworks;

import org.testng.annotations.BeforeTest;

import wdMethods.ProjectMethods;

public class TC003_DL_POM extends ProjectMethods{
	
	@BeforeTest
	public void setdata() {
		testCaseName = "TC003_DL_POM";
		testDescription ="Delete a lead in leaftaps";
		testNodes = "Leads";
		authors ="Dinesh";
		category = "sanity";
		dataSheetName="TC003_DL";
	}
}

