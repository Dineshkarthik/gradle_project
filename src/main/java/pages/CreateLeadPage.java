package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCname;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFname;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLname;
	@FindBy(how=How.ID, using="createLeadForm_primaryPhoneAreaCode") WebElement elePhcode;
	@FindBy(how=How.ID, using="createLeadForm_primaryPhoneNumber") WebElement elePhnum;
	@FindBy(how=How.ID, using="createLeadForm_primaryEmail") WebElement eleEmailaddress;
	@FindBy(how=How.NAME, using="submitButton") WebElement eleCLButton;
	
	@Given("enter company name as (.*)")
	public CreateLeadPage enterCompanyname(String data) {
	    type(eleCname, data);
	    return this;   
	}
	@Given("enter firstname as (.*)")
	public CreateLeadPage enterFirstname(String data) {
	    type(eleFname, data);
	    return this;   
	}
	@Given("enter lastname as (.*)")
	public CreateLeadPage enterLastname(String data) {
	    type(eleLname, data);
	    return this;   
	}
	
	public CreateLeadPage enterPhoneCode(String data) {
		type(elePhcode, data);
		return this;
	}
	public CreateLeadPage enterPhonenumber(String data) {
		type(elePhnum, data);
		return this;
	}
	@Given("click submit button")
	public ViewLeadPage clickSubmit() {
	    click(eleCLButton);
	    return new ViewLeadPage();   
	}
	public CreateLeadPage enterEmailId(String data) {
		type(eleEmailaddress, data);
		return this;
	}
	
}







