package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement eleFLead;
	@FindBy(how=How.LINK_TEXT, using="Edit") WebElement eleELead;
	@FindBy(how=How.LINK_TEXT, using="Delete") WebElement eleDLead;
	@FindBy(how=How.LINK_TEXT, using="Duplicate Lead") WebElement eleDPLead;
	@FindBy(how=How.ID, using="viewLead_companyName_sp") WebElement eleCname;
	@FindBy(how=How.ID, using="viewLead_firstName_sp") WebElement eleFname;
	@FindBy(how=How.XPATH, using="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a") WebElement eleFrecordFname;
	public FindLeadsPage clickFindLeads() {
	    click(eleFLead);
	    return new FindLeadsPage();   
	}
	public ViewLeadPage verifyPageTitle(String data) {
		verifyTitle(data);
		return this;
	}
	public EditLeadPage clickEdit() {
		click(eleELead);
		return new EditLeadPage();
	}
	public ViewLeadPage verifyCompanyname(String data) {
		verifyExactText(eleCname, data);
		return this;
	}
	public ViewLeadPage closewindow() {
		closeBrowser();
		return this;
	}
	public MyLeadsPage clickDelete() {
		click(eleDLead);
		return new MyLeadsPage();
	}
	public DuplicateLeadPage clickDuplicateLead() {
		click(eleDPLead);
		return new DuplicateLeadPage();
	}
	/*@Given("Lead is created")
	public ViewLeadPage verifyfname() {
		String text1=getText(eleFrecordFname);
		String text2 = getText(eleFname);
		if (text1.equals(text2)) {
			System.out.println("name matched");
		} else {
			System.out.println("name does not matched");
		}
		return this;
	}*/
	@Given("Lead is created")
	public ViewLeadPage verifyfname() {
		comparetwostrings(eleFname, eleFrecordFname);
		return this;
	}
	
}







