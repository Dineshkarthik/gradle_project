package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods{

	public LoginPage() {
		PageFactory.initElements(driver, this);  
	}
	//
	@FindBy(how=How.NAME, using="USERNAME") WebElement eleUname;
	@FindBy(how=How.ID, using="password") WebElement elePass;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;
	@Given("enter the username as (.*)")
	public LoginPage enterUsername(String data) {
		WebElement eleUname = locateElement("id", prop.getProperty("LoginPage.id.username"));   
		type(eleUname, data);
		System.out.println(this);
		return this;
	}
	@Given("enter the Password as (.*)")
	public LoginPage enterPassword(String data) {
		type(elePass, data);
	     System.out.println(this);
		return this;
	}
	@Given("click login button")
	public HomePage clickLogin() {
		click(eleLogin);
		//HomePage hp = new HomePage();
		//return hp;
		return new HomePage();
	}
}







