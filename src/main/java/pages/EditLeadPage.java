package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="updateLeadForm_companyName") WebElement eleCname;
	@FindBy(how=How.XPATH, using="//input[@class='smallSubmit'][1]") WebElement eleSubmit;
	
	public EditLeadPage enterCompanyname(String data) {
	    type(eleCname, data);
	    return new EditLeadPage();   
	}
	
	public ViewLeadPage clickUpdatebutton() {
	    click(eleSubmit);
	    return new ViewLeadPage();   
	}
}







