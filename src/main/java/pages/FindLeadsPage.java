package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH, using="//span[text()='Phone']") WebElement elePhoneTab;
	@FindBy(how=How.LINK_TEXT, using="Email") WebElement eleEmailTab;
	@FindBy(how=How.XPATH, using="//input[@name='phoneNumber']") WebElement elePhoneInput;
	@FindBy(how=How.NAME, using="emailAddress") WebElement eleEmailInput;
	@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]") WebElement eleFname;
	@FindBy(how=How.XPATH, using="//input[@name='id']") WebElement eleLeadId;
	@FindBy(how=How.XPATH, using="//input[@name='phoneAreaCode']") WebElement elePhoneCode;
	@FindBy(how=How.CLASS_NAME, using="x-paging-info") WebElement eleMessage;
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleFLbutton;
	@FindBy(how=How.XPATH, using="//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a") WebElement eleFrecordFname;
	@FindBy(how=How.XPATH, using="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a") WebElement eleFrecord;
	
	public FindLeadsPage enterFirstName(String data) {
	    type(eleFname, data);
	    return this;   
	}
	public FindLeadsPage clickFindLeadButton() {
	    click(eleFLbutton);
	    return this;   
	}
	public FindLeadsPage veirfyLead(String text) {
	    verifyExactText(eleFrecordFname, text);
	    return this;   
	}	
	
	public ViewLeadPage clickFirstresultID() {
	  click(eleFrecord);
	  return new ViewLeadPage();
	}
	public FindLeadsPage clickPhonetab() {
	  click(elePhoneTab);
	  return this;
	}
	
	public FindLeadsPage enterPhoneNumber(String data) {
		type(elePhoneInput, data);
		return this;
	}
	
	public FindLeadsPage enterPhoneCode(String data) {
		type(elePhoneCode, data);
		return this;
	}
	
	public FindLeadsPage captureLeadId() {
		String text = getText(eleFrecord);
		return this;
		}
	public FindLeadsPage enterCapturLeadId() {
		type(eleLeadId, getText(eleFrecord));
		return this;
	}
	public FindLeadsPage CheckMessage(String data) {
		verifyExactText(eleMessage, data);
		return this;
	}
	public FindLeadsPage clickEmailTab() {
		click(eleEmailTab);
		return this;
	}
	public FindLeadsPage enterEmailId(String data) {
		type(eleEmailInput, data);
		return this;
	}
}







