package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement eleCreateLead;
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement eleFindLead;
	
	@Given("click create lead link")
	public CreateLeadPage clickCreateLead() {
	    click(eleCreateLead);
	    return new CreateLeadPage();   
	}
	
	public FindLeadsPage clickFindLead() {
		click(eleFindLead);
		return new FindLeadsPage();
		
	}
	
}







