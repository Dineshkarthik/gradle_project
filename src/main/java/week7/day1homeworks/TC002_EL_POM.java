package week7.day1homeworks;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_EL_POM extends ProjectMethods{
	
	@BeforeTest
	public void setdata() {
		testCaseName = "TC002_EL_POM";
		testDescription ="Edit a lead in leaftaps";
		testNodes = "Leads";
		authors ="Dinesh";
		category = "sanity";
		dataSheetName="TC002_EL";
	}
	@Test(dataProvider="fetchData")
	public void editLead(String uname,String pwd,String fname,String verifytitle,String cname,String verifycname) {
		new LoginPage().enterUsername(uname).enterPassword(pwd).clickLogin().clickCRM().clickLeads().clickFindLead()
		.enterFirstName(fname).clickFindLeadButton().clickFirstresultID().verifyPageTitle(verifytitle).clickEdit()
		.enterCompanyname(cname)
		.clickUpdatebutton().verifyCompanyname(verifycname);
	}
}
