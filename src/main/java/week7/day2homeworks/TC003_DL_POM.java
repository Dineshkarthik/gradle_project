package week7.day2homeworks;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_DL_POM extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC003_DL_POM";
		testDescription ="Delete a lead in leaftaps";
		testNodes = "Leads";
		authors ="Dinesh";
		category = "sanity";
		dataSheetName="TC003_DL";
	}
	@Test(dataProvider="fetchData")
	public void deletelead(String uname, String pwd,String phcode,String phnum,String expectedmessage) {
		new LoginPage() 
		.enterUsername(uname)
		.enterPassword(pwd)  
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickFindLead()
		.clickPhonetab()
		.enterPhoneCode(phcode)
		.enterPhoneNumber(phnum)
		.clickFindLeadButton()
		.captureLeadId()
		.clickFirstresultID()
		.clickDelete()
		.clickFindLead()
		.enterCapturLeadId()
		.clickFindLeadButton()
		.CheckMessage(expectedmessage);	
	}
}











