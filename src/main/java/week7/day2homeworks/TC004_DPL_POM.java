package week7.day2homeworks;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_DPL_POM extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC004_DPL_POM";
		testDescription ="Duplicate lead creation in leaftaps";
		testNodes = "Leads";
		authors ="Dinesh";
		category = "sanity";
		dataSheetName="TC004_DPL";
	}
	@Test(dataProvider="fetchData")
	public void deletelead(String uname, String pwd,String emailid,String expectedtitle) {
		new LoginPage() 
		.enterUsername(uname)
		.enterPassword(pwd)  
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickFindLead()
		.clickEmailTab()
		.enterEmailId(emailid)
		.clickFindLeadButton()
		.clickFirstresultID()
		.clickDuplicateLead()
		.confirmTitle(expectedtitle)
		.clickSubmit()
		.verifyfname();
	}
}











