package week7.day1classworks;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CL_POM extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CL_POM";
		testDescription ="create a lead in leaftaps";
		testNodes = "Leads";
		authors ="Dinesh";
		category = "sanity";
		dataSheetName="TC001_CL";
	}
	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd,String cname,String fname,String lname,String phcode,String phnum, String email, String efname) {
		new LoginPage() 
		.enterUsername(uname)
		.enterPassword(pwd)  
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyname(cname)
		.enterFirstname(fname)
		.enterLastname(lname)
		.enterPhoneCode(phcode)
		.enterPhonenumber(phnum)
		.enterEmailId(email)
		.clickSubmit()
		.clickFindLeads()
		.enterFirstName(efname)
		.clickFindLeadButton()
		.veirfyLead(fname);
	}
}











