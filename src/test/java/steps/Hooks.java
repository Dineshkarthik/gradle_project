package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods{
	@Before
	public void beforescenario(Scenario sc) {
		startResult();
		startTestModule(sc.getName(), sc.getId());
		test = startTestCase("Nodes");
		test.assignCategory("Smoke");
		test.assignAuthor("Dinesh");
		startApp("chrome", "http://leaftaps.com/opentaps");		
	}
	@After
	public void afterscenario() {
		closeAllBrowsers();
		endResult();
	}
}
